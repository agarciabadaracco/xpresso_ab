import asyncio
import logging
import os
from asyncio import AbstractEventLoop
from contextlib import asynccontextmanager
from pathlib import Path
from typing import AsyncGenerator, AsyncIterator, Iterator, cast
from urllib.parse import urlparse, urlunparse

import pytest
import pytest_asyncio
from asgi_lifespan import LifespanManager
from buildpg import V
from buildpg.asyncpg import connect_b
from dotenv import load_dotenv
from httpx import AsyncClient
from pydantic import PostgresDsn
from xpresso import App

from src.app import create_app
from src.dependencies import get_app_settings
from src.settings import AppSettings

logger = logging.getLogger(__name__)


@pytest.fixture(scope="session")
def env_test_file() -> Path:
    if os.getenv("CI") == "true":
        env_file = Path(__file__).parent / ".env_ci_gitlab"
        logger.info("CI env file loaded")
    else:
        env_file = Path(__file__).parent / ".env_test"
        logger.info("Local test env file loaded")
    assert env_file.exists()
    return env_file


@pytest.fixture(scope="session")
def app_settings_test(env_test_file: Path) -> AppSettings:
    load_dotenv(env_test_file)
    _app_settings = get_app_settings()
    return _app_settings


@pytest_asyncio.fixture(scope="session")
async def app_test(app_settings_test: AppSettings) -> AsyncIterator[App]:
    async with create_test_database(app_settings_test.db, "xpresso_test") as dsn_test:
        app_settings_test.db = cast(PostgresDsn, dsn_test)
        app = create_app(app_settings_test)
        async with LifespanManager(app):
            yield app


@pytest.fixture(scope="session")
def event_loop() -> Iterator[AbstractEventLoop]:
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest_asyncio.fixture(scope="session")
async def client(app_test: App) -> AsyncIterator[AsyncClient]:
    async with AsyncClient(app=app_test, base_url="https://testserver") as c:
        yield c


class DatabaseTestCreationError(Exception):
    pass


class DatabaseTestFeedError(Exception):
    pass


@asynccontextmanager
async def create_test_database(dsn: str, dbname: str) -> AsyncGenerator[str, None]:
    logger.debug(f"Create test database: {dbname} using dsn: {dsn}")
    conn = await connect_b(
        dsn=dsn, server_settings={"application_name": "xpresso_ab_tests"}
    )
    try:
        response = await conn.execute_b("create database :dbname", dbname=V(dbname))
        assert response == "CREATE DATABASE"
        logger.debug(f"yield create_test_database: {dbname}")
        parsed = urlparse(dsn)
        replaced = parsed._replace(path=dbname)
        dsn_unparse = urlunparse(replaced)
        yield dsn_unparse
        logger.debug(f"end yield create_test_database: {dbname}")
    except Exception as e:
        logger.exception(e)
        raise DatabaseTestCreationError from e
    finally:
        # drop database here
        logger.debug(f"finally create_test_database: {dbname}")
        response = await conn.execute_b("drop database :dbname", dbname=V(dbname))
        await conn.close()
        assert response == "DROP DATABASE"
        logger.debug(f"end create_test_database: {dbname}")
