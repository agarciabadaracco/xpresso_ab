from src.settings import AppSettings


def test_settings_loaded(app_settings_test: AppSettings) -> None:
    assert app_settings_test.debug is True
