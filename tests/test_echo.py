import pytest
from httpx import AsyncClient


@pytest.mark.asyncio
async def test_echo(client: AsyncClient) -> None:
    response = await client.get("/echo/dburl")
    assert response.text == '"xpresso_test"'
