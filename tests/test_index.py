import pytest
from httpx import AsyncClient


@pytest.mark.asyncio
async def test_index(client: AsyncClient) -> None:
    response = await client.get("/")
    assert "session data" in response.text
