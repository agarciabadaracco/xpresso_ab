import json
from datetime import datetime

from starlette.requests import Request
from starlette.responses import HTMLResponse, RedirectResponse
from xpresso import Path


async def homepage(request: Request) -> HTMLResponse:
    """Access this view (GET "/") to display session contents."""
    return HTMLResponse(
        f"<div>session data: {json.dumps(request.session)}</div>"
        "<ol>"
        '<li><a href="/set_time">set example data</a></li>'
        '<li><a href="/clear_time">clear example data</a></li>'
        "</ol>"
    )


async def raise_exc() -> str:
    raise TypeError("Oops, something really went wrong...")


async def set_time(request: Request) -> RedirectResponse:
    """Access this view (GET "/set") to set session contents."""
    request.session["date"] = datetime.now().isoformat()
    return RedirectResponse("/")


async def clear_time(request: Request) -> RedirectResponse:
    """Access this view (GET "/clean") to remove all session contents."""
    request.session.clear()
    return RedirectResponse("/")


routes: list[Path] = [
    Path(
        "/",
        get=homepage,
    ),
    Path(
        "/exception",
        get=raise_exc,
    ),
    Path("/set_time", get=set_time),
    Path("/clear_time", get=clear_time),
]
