from xpresso import Path

from src.dependencies import MyBuildPgConnection

routes: list[Path] = []


async def echo_db_url(
    conn: MyBuildPgConnection,
) -> str:
    resp = await conn.fetchval_b("select current_database()")
    return resp


routes.append(
    Path(
        "/echo/dburl",
        get=echo_db_url,
    )
)
