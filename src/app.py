import logging

from starsessions import CookieStore, SessionAutoloadMiddleware, SessionMiddleware
from xpresso import App, Path
from xpresso.middleware import Middleware

from src.dependencies import get_app_settings
from src.routes.echo import routes as echo_routes
from src.routes.index import routes as index_routes
from src.settings import AppSettings

logger = logging.getLogger(__name__)


def create_app(app_settings: AppSettings | None = None) -> App:
    if not app_settings:
        app_settings = get_app_settings()

    all_routes: list[Path] = []
    for routes in [echo_routes, index_routes]:
        for r in routes:
            all_routes.append(r)
    session_store = CookieStore(secret_key="TOP SECRET")

    middleware = [
        Middleware(SessionMiddleware, store=session_store),
        Middleware(SessionAutoloadMiddleware),
    ]
    app = App(routes=all_routes, debug=app_settings.debug, middleware=middleware)
    return app
