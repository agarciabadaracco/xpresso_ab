from pydantic import BaseSettings, PostgresDsn


class AppSettings(BaseSettings):
    db: PostgresDsn
    debug: bool = False

    class Config:
        env_file = ".env"
