import logging
from functools import lru_cache
from typing import Annotated, AsyncGenerator

from buildpg.asyncpg import BuildPgConnection, BuildPgPool, create_pool_b
from xpresso import Depends

from src.settings import AppSettings

logger = logging.getLogger(__name__)


@lru_cache
def get_app_settings() -> AppSettings:
    return AppSettings()


MyAppSettings = Annotated[AppSettings, Depends(get_app_settings, scope="app")]


async def build_db_pool(
    app_settings: MyAppSettings,
) -> AsyncGenerator[BuildPgPool, None]:
    _pool = await create_pool_b(app_settings.db)
    yield _pool
    await _pool.close()


MyBuildPgPool = Annotated[BuildPgPool, Depends(build_db_pool, scope="app")]


async def get_connection(
    pool: MyBuildPgPool,
) -> AsyncGenerator[BuildPgConnection, None]:
    logger.debug("DB connection in")
    async with pool.acquire() as conn:
        yield conn
    logger.debug("DB connection out")


MyBuildPgConnection = Annotated[BuildPgConnection, Depends(get_connection)]
