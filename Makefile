.DEFAULT_GOAL := help

PY_SRC := src
PY_SRC_TEST := tests

.PHONY: all
all: format check test report ## All operations : format check test report

.PHONY: check  ## Check black flake8 isort mypy
check: check-black check-flake8 check-isort check-mypy

.PHONY: check-black
check-black:  ## Check if code is formatted nicely using black.
	black --check --diff $(PY_SRC) $(PY_SRC_TEST)

.PHONY: check-flake8
check-flake8:  ## Check for general warnings in code using flake8.
	flake8 --exclude=.venv $(PY_SRC) $(PY_SRC_TEST)

.PHONY: check-mypy
check-mypy:  ## Check for general warnings in code using mypy.
	mypy $(PY_SRC) $(PY_SRC_TEST)

.PHONY: check-isort
check-isort:  ## Check for general warnings in code using isort.
	isort --check --diff $(PY_SRC) $(PY_SRC_TEST)

.PHONY: clean
clean: ## Delete temporary files.
	@rm -rf build 2>/dev/null
	@rm -rf dist 2>/dev/null
	@rm -rf .coverage* 2>/dev/null
	@rm -rf .pytest_cache 2>/dev/null
	@rm -rf $(PY_SRC_TEST)/.pytest_cache 2>/dev/null
	@rm -rf .mypy_cache 2>/dev/null
	@rm -rf pip-wheel-metadata 2>/dev/null
	@rm -rf htmlcov 2>/dev/null

.PHONY: help
help:  ## Print this help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z0-9_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST) | sort

.PHONY: lint-black
lint-black:  ## Lint the code using black.
	black $(PY_SRC) $(PY_SRC_TEST)

.PHONY: format
format: ## use black and isort to format code
	black $(PY_SRC) $(PY_SRC_TEST)
	isort $(PY_SRC) $(PY_SRC_TEST)

.PHONY: test
test: ## Run the tests using coverage with pytest module.
	coverage run

.PHONY: report
report: ## coverage report
	coverage report
	coverage html

.PHONY: setup
setup:  ## Setup the development environment.
	poetry install


